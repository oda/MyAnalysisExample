#-----------------------------------------------------------------------------
# Athena imports
#-----------------------------------------------------------------------------
from AthenaCommon.Constants import *
from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

#-----------------------------------------------------------------------------
# Message Service
#-----------------------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = WARNING
import AthenaServices
AthenaServices.AthenaServicesConf.AthenaEventLoopMgr.OutputLevel = WARNING

#-----------------------------------------------------------------------------
# Input Datasets
#-----------------------------------------------------------------------------
ServiceMgr.EventSelector.InputCollections = [
    "/home/oda/data2082d/tutorial2019/DxAODs/data18_13TeV.00352448.physics_Main.deriv.DAOD_STDM3.f938_m1979_p3583/DAOD_STDM3.14503110._000001.pool.root.1"
]
theApp.EvtMax = 10 # -1 means all events

#-----------------------------------------------------------------------------
# Algorithms
#-----------------------------------------------------------------------------
from MyAnalysisExample.MyAnalysisExampleConf import *

job += MyAnalysisAlg("MyAnalysisAlg1", OutputLevel = INFO)
job += MyAnalysisAlg("MyAnalysisAlg2", OutputLevel = FATAL)
job += MyAnalysisAlg("MyAnalysisAlg3", OutputLevel = INFO,
                     message = "But if you try sometimes, you just might find you get what you need.")

print job
#-----------------------------------------------------------------------------
