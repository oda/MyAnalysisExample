#include "MyAnalysisExample/MyAnalysisAlg.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"

#include "GaudiKernel/ITHistSvc.h"
#include "TH1.h"
#include "TH2.h"

MyAnalysisAlg::MyAnalysisAlg(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator)
  , m_thistSvc("THistSvc/THistSvc", name)
{
  declareProperty("message", m_message="You cannot always get what you want.");
}

StatusCode MyAnalysisAlg::initialize() {
  ATH_MSG_INFO("initialize()");
  ATH_MSG_INFO("My message: " << m_message);

  ATH_CHECK(m_thistSvc.retrieve());

  m_h_nMuons = new TH1D("h_nMuons", "Number of muons", 11, -0.5, 10.5);
  m_thistSvc->regHist("/MyAnalysisAlg/Muon/h_nMuons", m_h_nMuons).setChecked();
  m_h_muon_e = new TH1D("h_muon_e", "Muon energy [GeV]", 100, 0., 250.);
  m_thistSvc->regHist("/MyAnalysisAlg/Muon/h_muon_e", m_h_muon_e).setChecked();
  m_h_muon_pt = new TH1D("h_muon_pt", "Muon p_{T} [GeV]", 100, 0., 250.);
  m_thistSvc->regHist("/MyAnalysisAlg/Muon/h_muon_pt", m_h_muon_pt).setChecked();
  m_h_muon_eta = new TH1D("h_muon_eta", "Muon #eta", 100, -3.0, 3.0);
  m_thistSvc->regHist("/MyAnalysisAlg/Muon/h_muon_eta", m_h_muon_eta).setChecked();
  m_h_muon_phi = new TH1D("h_muon_phi", "Muon #phi [radian]", 100, -M_PI, M_PI);
  m_thistSvc->regHist("/MyAnalysisAlg/Muon/h_muon_phi", m_h_muon_phi).setChecked();
  m_h2_muon_eta_phi = new TH2D("h2_muon_eta_phi", "Muon #eta vs #phi", 100, -3.0, 3.0, 100, -M_PI, M_PI);
  m_thistSvc->regHist("/MyAnalysisAlg/Muon/h2_muon_eta_phi", m_h2_muon_eta_phi).setChecked();

  return StatusCode::SUCCESS;
}

StatusCode MyAnalysisAlg::finalize() {
  ATH_MSG_INFO("finalize()");
  return StatusCode::SUCCESS;
}

StatusCode MyAnalysisAlg::execute() {
  ATH_MSG_INFO("execute()");

  // Event information
  const xAOD::EventInfo* eventInfo = nullptr;
  ATH_CHECK(evtStore()->retrieve(eventInfo));
  uint32_t runNumber = eventInfo->runNumber();
  uint32_t lumiBlock = eventInfo->lumiBlock();
  unsigned long long eventNumber = eventInfo->eventNumber();
  ATH_MSG_INFO("Run = " << runNumber << " : LB " << lumiBlock << " : Event = " << eventNumber);

  // Muon
  const xAOD::MuonContainer* muonContainer = nullptr;
  ATH_CHECK(evtStore()->retrieve(muonContainer, "Muons"));
  ATH_MSG_DEBUG("Muons successfully retrieved");
  ATH_MSG_DEBUG("# of muons = " << muonContainer->size());

  unsigned int nMuons = 0;
  for (const xAOD::Muon* muon : *muonContainer) {
    ATH_MSG_INFO(nMuons << "th muon :" <<
		 " Pt : " << muon->pt() <<
		 " Eta : " << muon->eta() <<
		 " Phi : " << muon->phi() <<
		 " E : " << muon->e() <<
		 " M : " << muon->m());
    m_h_muon_e->Fill(muon->e()/1000.); // Energy in GeV
    m_h_muon_pt->Fill(muon->pt()/1000.); // Transverse momentum in GeV
    m_h_muon_eta->Fill(muon->eta()); // Pseudorapidity
    m_h_muon_phi->Fill(muon->phi()); // Azimuthal angle
    m_h2_muon_eta_phi->Fill(muon->eta(), muon->phi()); // 2D distribution, X:eta, Y:phi 

    ++nMuons;
  }

  m_h_nMuons->Fill(nMuons); // The number of muons

  return StatusCode::SUCCESS;
}
