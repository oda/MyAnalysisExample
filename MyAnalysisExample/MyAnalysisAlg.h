// -*- C++ -*-

#ifndef MYANALYSISALG_H // Include guard
#define MYANALYSISALG_H

#include "AthenaBaseComps/AthAlgorithm.h"

class ITHistSvc;
class TH1D;
class TH2D;

class MyAnalysisAlg:public AthAlgorithm
{
 public:
  MyAnalysisAlg(const std::string& name, ISvcLocator* pSvcLocator); // Constructor

  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override;
  virtual StatusCode execute() override;

 private:
  std::string m_message;
  ServiceHandle<ITHistSvc> m_thistSvc;
  TH1D* m_h_nMuons;
  TH1D* m_h_muon_e;
  TH1D* m_h_muon_pt;
  TH1D* m_h_muon_eta;
  TH1D* m_h_muon_phi;
  TH2D* m_h2_muon_eta_phi;
};

#endif // MYANALYSISALG_H
